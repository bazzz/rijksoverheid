package rijksoverheid

import (
	"testing"
)

func TestGetVacations(t *testing.T) {
	vacations, err := GetVacations()
	if err != nil {
		t.Fatal(err)
	}
	if len(vacations) <= 0 {
		t.Fatal("vacations is empty")
	}
}
