package rijksoverheid

import (
	"encoding/xml"
	"io"
	"net/http"
	"strings"
	"time"
)

const baseURL = "https://opendata.rijksoverheid.nl/v1/sources"

// GetVacations returns the upcoming vacations.
func GetVacations() ([]Vacation, error) {
	result := []Vacation{}
	url := baseURL + "/rijksoverheid/infotypes/schoolholidays"
	response, err := http.Get(url)
	if err != nil {
		return result, err
	}
	defer response.Body.Close()
	data, err := io.ReadAll(response.Body)
	if err != nil {
		return result, err
	}
	var doc documents
	err = xml.Unmarshal(data, &doc)
	if err != nil {
		return result, err
	}
	now := time.Now()
	for _, d := range doc.Documents {
		for _, v := range d.Content.ContentBlock.Vacations.Vacations {
			for _, r := range v.Regions {
				if r.End.Sub(now) <= 0 {
					continue // Skip vacations that have already passed.
				}
				newVacation := Vacation{
					Name:   clean(v.Type),
					Region: clean(r.Region),
					Start:  r.Start.Time.Add(24 * time.Hour), // Correct off-by-one so that a week is Sunday - Sunday.
					End:    r.End.Time,
				}
				result = append(result, newVacation)
			}
		}
	}
	return result, nil
}

func clean(input string) string {
	return strings.Trim(input, " \n\t")
}
