package rijksoverheid

type documents struct {
	Documents []document `xml:"document"`
}

type document struct {
	Content content `xml:"content"`
}

type content struct {
	ContentBlock contentblock `xml:"contentblock"`
}

type contentblock struct {
	Vacations vacations `xml:"vacations"`
}
