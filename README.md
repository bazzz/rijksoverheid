# rijksoverheid

Package rijksoverheid provides a client to access the public Rijksoverheid API at https://opendata.rijksoverheid.nl. For now it only consumes the School Vacations endpoint, simply because I needed just that one. More may come in the future... If you use this package and have added other endpoints, please make a merge request.

## usage

Simple example usage:

    vacations, err := rijksoverheid.GetVacations()
    if err != nil {
        log.Fatal(err)
    }
    // Do something with vacations

## installation

You can add package rijksoverheid to your project with a simple import. It is written in pure Go and has no dependencies.
