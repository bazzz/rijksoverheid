package rijksoverheid

import "time"

type vacations struct {
	Vacations []vacation `xml:"vacation"`
}

type vacation struct {
	Type    string   `xml:"type"`
	Regions []region `xml:"regions"`
}

type region struct {
	Region string  `xml:"region"`
	Start  XMLDate `xml:"startdate"`
	End    XMLDate `xml:"enddate"`
}

// Vacation represents a school vacation in the Netherlands. The region specifies whether it is for the whole country or for north/centre/south.
type Vacation struct {
	Name   string
	Region string
	Start  time.Time
	End    time.Time
}
