package rijksoverheid

import (
	"encoding/xml"
	"time"
)

// XMLDate is a holder for a standard go time struct that can be parsed from XML.
type XMLDate struct {
	time.Time
}

func (c *XMLDate) UnmarshalXML(d *xml.Decoder, start xml.StartElement) error {
	const shortForm = "2006-01-02"
	var v string
	d.DecodeElement(&v, &start)
	parse, err := time.Parse(shortForm, v[:10])
	if err != nil {
		return err
	}
	*c = XMLDate{parse}
	return nil
}
